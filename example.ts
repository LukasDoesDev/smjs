import { nBitAdderWithHumanInterface } from "./src/bitLogic.ts";
import { buildBlueprint } from "./src/core.ts";

console.log(
  JSON.stringify(
    buildBlueprint(
      nBitAdderWithHumanInterface(4, {
        x: 0,
        y: 0,
        z: 0,
      }),
    ),
    null,
    2,
  ),
);
