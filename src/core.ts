import { addPositions, Position } from "./position.ts";

export interface Blueprint {
  bodies: {
    childs: BodyChild[];
  }[];
}
export interface BodyChild {
  shape?: unknown;
  color?: string;
  controller?: {
    color?: string;
    coneAngle?: number;
    luminance?: number;
    active?: boolean;
    controllers?: {
      id: number;
    }[];
    id: number;
    joints?: unknown[];
    mode?: number;
  };
  pos: Position;
  shapeId: ShapeId | string;
}
export interface LogicBodyChild extends BodyChild {
  controller: {
    active: boolean;
    controllers: {
      id: number;
    }[];
    id: number;
    mode: number;
  };
}

export const idManager = (function () {
  var currentId = 220;
  return {
    incrementId(times: number) {
      currentId += times;
    },
    newId(): number {
      return currentId++;
    },
    multipleNew(n: number): number[] {
      return Array.from({ length: n }, () => idManager.newId());
    },
  };
})();

export enum ShapeId {
  Logic = "9f0f56e8-2c31-4d83-996c-d00a9b296c3f",
  Switch = "7cf717d7-d167-4f2d-a6e7-6b2c70aa3986",
  Headlight = "ed27f5e2-cac5-4a32-a5d9-49f116acc6af",
}
export enum LogicType {
  And = 0,
  Or = 1,
  Xor = 2,
  Nand = 3,
  Nor = 4,
  Xnor = 5,
}

export interface FourBitsIdsRecord {
  a: number;
  b: number;
  c: number;
  d: number;
}

export interface OneBitAdderReturn {
  ids: {
    a: number;
    b: number;
    carryIn: number;

    carryOut: number;
    sum: number;
  };
  parts: BodyChild[];
}

export function createLogic(
  id: number | null,
  connections: number[],
  pos: Position,
  mode: LogicType = LogicType.And,
  color = "df7f01",
  extra = {},
): LogicBodyChild {
  return {
    color: color,
    controller: {
      active: false,
      controllers: connections.map((c) => ({ id: c })),
      id: id || idManager.newId(),
      mode: mode,
    },
    pos: pos,
    shapeId: ShapeId.Logic,
    ...extra,
    /* xaxis: 1,
    zaxis: -2 */
  };
}

export function createSwitch(
  id: number | null,
  connections: number[],
  pos: Position,
  color = "df7f01",
  extra = {},
): BodyChild {
  return {
    color: color,
    controller: {
      active: false,
      controllers: connections.map((c) => ({ id: c })),
      id: id || idManager.newId(),
    },
    pos: pos,
    shapeId: ShapeId.Switch,
    ...extra,
  };
}

export function createHeadlight(
  id: number | null,
  pos: Position,
  luminance = 100,
  coneAngle = 0,
  color = "df7f01",
  extra = {},
): BodyChild {
  return {
    color: color,
    controller: {
      color: color,
      coneAngle: coneAngle,
      id: id || idManager.newId(),
      luminance: luminance,
    },
    pos: pos,
    shapeId: ShapeId.Headlight,
    ...extra,
  };
}

export function buildBlueprint(children: BodyChild[]): Blueprint {
  return {
    bodies: [
      {
        childs: children,
      },
    ],
  };
}

export function oneBitAdder(
  position: Position,
  carryOutIds: number[] = [],
  sumOutIds: number[] = [],
  multiplePositions = false,
): OneBitAdderReturn {
  const ids = idManager.multipleNew(10);
  const a = createLogic(ids[0], [ids[3], ids[4]], position);
  const b = createLogic(
    ids[1],
    [ids[3], ids[4]],
    multiplePositions ? addPositions(position, { x: 0, y: 1, z: 0 }) : position,
  );
  const carryIn = createLogic(
    ids[2],
    [ids[7], ids[5]],
    multiplePositions ? addPositions(position, { x: 0, y: 2, z: 0 }) : position,
  );

  const unnamed1 = createLogic(
    ids[3],
    [ids[6]],
    multiplePositions ? addPositions(position, { x: 0, y: 3, z: 0 }) : position,
  );
  const unnamed2 = createLogic(
    ids[4],
    [ids[5], ids[7]],
    multiplePositions ? addPositions(position, { x: 0, y: 4, z: 0 }) : position,
    LogicType.Xor,
  );
  const unnamed3 = createLogic(
    ids[5],
    [ids[9]],
    multiplePositions ? addPositions(position, { x: 0, y: 5, z: 0 }) : position,
    LogicType.Xor,
  );

  const unnamed4 = createLogic(
    ids[6],
    [ids[8]],
    multiplePositions ? addPositions(position, { x: 0, y: 6, z: 0 }) : position,
    LogicType.Or,
  );
  const unnamed5 = createLogic(
    ids[7],
    [ids[6]],
    multiplePositions ? addPositions(position, { x: 0, y: 7, z: 0 }) : position,
  );

  const carryOut = createLogic(
    ids[8],
    carryOutIds,
    multiplePositions ? addPositions(position, { x: 0, y: 8, z: 0 }) : position,
  );
  const sum = createLogic(
    ids[9],
    sumOutIds,
    multiplePositions ? addPositions(position, { x: 0, y: 9, z: 0 }) : position,
  );
  return {
    ids: {
      a: a.controller.id,
      b: b.controller.id,
      carryIn: carryIn.controller.id,

      carryOut: carryOut.controller.id,
      sum: sum.controller.id,
    },
    parts: [
      a,
      b,
      carryIn,
      unnamed1,
      unnamed2,
      unnamed3,
      unnamed4,
      unnamed5,
      carryOut,
      sum,
    ],
  };
}

export function nBitAdder(
  n: number,
  position: Position,
  carryOutIds: number[],
  sumOutIds: number[][],
): {
  ids: {
    a: number[];
    b: number[];
    carryIn: number;
  };
  parts: BodyChild[];
} {
  const adders: OneBitAdderReturn[] = [];
  for (let index = 0; index < n; index++) {
    adders.push(
      oneBitAdder(
        position,
        index === 0 ? carryOutIds : [adders.at(-1)!.ids.a],
        sumOutIds[index],
      ),
    );
  }
  return {
    ids: {
      carryIn: adders[0].ids.a,
      a: adders.map((x) => x.ids.b),
      b: adders.map((x) => x.ids.carryIn),
    },
    parts: [...adders.flatMap((x) => x.parts)],
  };
}
