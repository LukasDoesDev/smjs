export interface Position {
  x: number;
  y: number;
  z: number;
}

export function addPositions(a: Position, b: Position) {
  return { x: a.x + b.x, y: a.y + b.y, z: a.z + b.z };
}
