import {
  createHeadlight,
  createLogic,
  createSwitch,
  idManager,
  LogicType,
  nBitAdder,
} from "./core.ts";
import { addPositions, Position } from "./position.ts";

export function nBitAdderWithInterface(
  n: number,
  position: Position,
  multiplePositions = false,
) {
  const carryInId = idManager.newId();
  const aIds = idManager.multipleNew(n);
  const carryOutId = idManager.newId();
  const bIds = idManager.multipleNew(n);
  const cIds = idManager.multipleNew(n);
  const adder = nBitAdder(n, position, [carryInId], [...cIds.map((x) => [x])]);
  return [
    createLogic(
      carryInId,
      [],
      addPositions(position, { x: 0, z: 1, y: 0 }),
      LogicType.And,
      "00ffff",
    ), // Cyan

    // Yellow
    ...Array.from({ length: n }, (_v, i) =>
      createLogic(
        cIds[i],
        [],
        multiplePositions
          ? addPositions(position, { x: 0, z: i + 2, y: 0 })
          : position,
        LogicType.And,
        "ffff00",
      )),

    ...adder.parts,

    createLogic(
      carryOutId,
      [adder.ids.carryIn],
      addPositions(position, { x: 0, z: 1, y: 1 }),
      LogicType.And,
      "0000ff",
    ), // Blue

    // Red
    ...Array.from({ length: n }, (_v, i) =>
      createLogic(
        aIds[i],
        [adder.ids.a[i]],
        multiplePositions
          ? addPositions(position, { x: 0, z: i + 2, y: 1 })
          : position,
        LogicType.And,
        "ff0000",
      )),

    // Green
    ...Array.from({ length: n }, (_v, i) =>
      createLogic(
        bIds[i],
        [adder.ids.b[i]],
        multiplePositions
          ? addPositions(position, { x: 0, z: i + 2, y: 2 })
          : position,
        LogicType.And,
        "00ff00",
      )),
  ];
}
export function nBitAdderWithHumanInterface(
  n: number,
  position: Position,
  multiplePositions = false,
) {
  const carryInId = idManager.newId();
  const carryInHeadlightId = idManager.newId();
  const aIds = idManager.multipleNew(n);
  const aSwitchIds = idManager.multipleNew(n);
  const carryOutId = idManager.newId();
  // TODO: const carryOutSwitchId = idManager.newId()
  const bIds = idManager.multipleNew(n);
  const bSwitchIds = idManager.multipleNew(n);
  const cIds = idManager.multipleNew(n);
  const cHeadlightIds = idManager.multipleNew(n);

  const adder = nBitAdder(
    n,
    { x: 0, y: 0, z: 0 },
    [carryInId],
    [...cIds.map((x) => [x])],
  );
  return [
    createLogic(
      carryInId,
      [carryInHeadlightId],
      multiplePositions
        ? addPositions(position, { x: 0, z: 1, y: 0 })
        : position,
      LogicType.And,
      "00ffff",
    ), // Cyan

    createHeadlight(
      carryInHeadlightId,
      addPositions(position, { x: 1, z: 2, y: 0 }),
      100,
      0,
      "00ffff",
      {
        xaxis: -3,
        zaxis: -1,
      },
    ),

    // Yellow
    ...Array.from({ length: n }, (_v, i) =>
      createLogic(
        cIds[i],
        [cHeadlightIds[i]],
        multiplePositions
          ? addPositions(position, { x: 0, z: i + 2, y: 0 })
          : position,
        LogicType.And,
        "ffff00",
      )),
    ...Array.from({ length: n }, (_v, i) =>
      createHeadlight(
        cHeadlightIds[i],
        addPositions(position, { x: 1, z: i + 3, y: 0 }),
        100,
        0,
        "ffff00",
        {
          xaxis: -3,
          zaxis: -1,
        },
      )),

    ...adder.parts,

    createLogic(
      carryOutId,
      [adder.ids.carryIn],
      multiplePositions ? addPositions(position, { x: 0, z: 1, y: 1 })
      : position,
      LogicType.And,
      "0000ff",
    ), // Blue

    // Red
    ...Array.from({ length: n }, (_v, i) =>
      createLogic(
        aIds[i],
        [adder.ids.a[i]],
        multiplePositions
          ? addPositions(position, { x: 0, z: i + 2, y: 1 })
          : position,
        LogicType.And,
        "ff0000",
      )),
    ...Array.from({ length: n }, (_v, i) =>
      createSwitch(
        aSwitchIds[i],
        [aIds[i]],
        addPositions(position, { x: 0, z: i + 2, y: 1 }),
        "ff0000",
        {
          xaxis: 2,
          zaxis: 3,
        },
      )),

    // Green
    ...Array.from({ length: n }, (_v, i) =>
      createLogic(
        bIds[i],
        [adder.ids.b[i]],
        multiplePositions
          ? addPositions(position, { x: 0, z: i + 2, y: 2 })
          : position,
        LogicType.And,
        "00ff00",
      )),
    ...Array.from({ length: n }, (_v, i) =>
      createSwitch(
        bSwitchIds[i],
        [bIds[i]],
        addPositions(position, { x: 0, z: i + 2, y: 2 }),
        "00ff00",
        {
          xaxis: 2,
          zaxis: 3,
        },
      )),
  ];
}
